package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.players;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game.Tile;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game.Game;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Player which plays the game without an person to control him
 * @author Petr Wudi
 */
public class AIPlayer extends Player implements Runnable {
    
    /**
     * Maximum possible skill player could have
     */
    protected static final int MAX_SKILL = 2;
    
    /**
     * Player's delay relative to level - difference between two speed of two
     * players with skill difference of 1.
     */
    protected static final int PLAYER_SPEED_RELATIVE = 2000;
    
    /**
     * Minimum time player needs to think about the game (does no depend on skill)
     */
    protected static final int PLAYER_SPEED_ABSOLUTE = 1000;
    
    /**
     * Maximum time randomly added to thinking delay. It ensures time between
     * player's actions is not always the same.
     */
    protected static final int PLAYER_SPEED_VARIABLE = 1000;
    
    /**
     * true if player can still play game, false if stepped on bomb
     */
    boolean isAlive = true;
    
    /**
     * Tile that was chosen last time
     */
    Tile lastChosenTile;
    
    /**
     * delay between player's actions
     */
    int waitingTime;
    
    /**
     * width of game player plays
     */
    int gameWidth;
    
    /**
     * height of game player plays
     */
    int gameHeight;
    
    public AIPlayer(Game game, int skill, int width, int height) {
        super(game);
        waitingTime = (MAX_SKILL - skill) * PLAYER_SPEED_RELATIVE + PLAYER_SPEED_ABSOLUTE;
        gameWidth = width;
        gameHeight = height;
        lastChosenTile = null;
    }

    @Override
    public synchronized void run() {
        while(isAlive){
            thinkUselessly();
            discoverTile();
        }
    }
    
    /**
     * Discovers some tile
     */
    protected void discoverTile(){
        Tile chosenTile = (lastChosenTile == null) ?
                null : lastChosenTile.neighbourFreeNotBomb();
        if(chosenTile == null){
            chosenTile = randomNotBombTile();
        }
        lastChosenTile = chosenTile;
        int x = chosenTile.getXPosition();
        int y = chosenTile.getYPosition();
        if(chosenTile.containsBomb()){
            chosenTile.markBomb();
            game.announceBomb(x, y);
        }
        else{
            chosenTile.goHere(this);
            game.discover(x, y);
        }
        game.announceDiscovery(x, y);
        ++score;
    }
    
    /**
     * Finds an tile that does not contain bomb
     * @return not bombed random tile
     */
    protected Tile randomNotBombTile(){
        Tile tile;
        boolean containsBomb;
        boolean isDiscovered;
        int ndr;
        do{
            tile = randomTile();
            containsBomb = tile.containsBomb();
            ndr = tile.neighbourDiscoveredRanking();
            isDiscovered = tile.isDiscovered();
            if(containsBomb && !isDiscovered && ndr > 8){
                break;
            }
        }
        while(containsBomb || isDiscovered || ndr < 5);
        return tile;
    }
    
    /**
     * Randomly chooses an tile on grid
     * @return random tile
     */
    protected Tile randomTile(){
        int x = (int) (gameWidth * Math.random());
        int y = (int) (gameHeight * Math.random());
        return game.getTile(x, y);
    }
    
    /**
     * Wastest time thinking about the game
     */
    private synchronized void thinkUselessly(){
        try {
            wait((long) (waitingTime + PLAYER_SPEED_VARIABLE * Math.random()));
        }
        catch (InterruptedException ex) {
            Logger.getLogger(AIPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Stops the player
     */
    public void stop(){
        isAlive = false;
    }
    
}
