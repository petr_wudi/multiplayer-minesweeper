/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.players;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game.Tile;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game.Game;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game.MoveDirection;

/**
 *
 * @author Petr
 */
public class Player {
    
    Game game;
    int score = 0;

    public Player(Game game) {
        this.game = game;
    }
    
    /**
     * Kills the player
     */
    public void die(){
        score = -1;
        game.setLost();
    }
    
    /**
     * Moves player in specified direction
     * @param direction direction to move
     */
    public void move(MoveDirection direction){
        game.movePlayer(direction, this);
    }

    /**
     * Marks tile in specified direction as bomb
     * @param direction direction in which bombs lies
     */
    public void markBomb(MoveDirection direction) {
        game.markBomb(direction, this);
    }

    /**
     * Moves player to specified tile
     * @param tile tile to move player on
     */
    public void goTo(Tile tile) {
        addScore(tile);
        tile.goHere(this);
        game.announceDiscovery(tile.getXPosition(), tile.getYPosition());
    }

    /**
     * Marks tile as containing bomb
     * @param tile tile to mark
     */
    public void markMine(Tile tile) {
        if(tile.containsBomb()){
            addScore(tile);
        }
        game.announceBomb(tile.getXPosition(), tile.getYPosition());
        tile.markBomb();
        tile.goHere(this);
    }
    
    /**
     * @return number of tiles player found
     */
    public int getScore(){
        return score;
    }
    
    /**
     * Takes score from specified tile. If tile is empty, scoree won't increase
     * @param tile tile player discovered
     */
    private void addScore(Tile tile){
        if(!tile.isDiscovered()){ 
            ++score;
        }
    }
}
