package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.networking;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game.Game;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class which controls all player instructions. All changes in this game must
 * flow through this class. It receives messages from all another players
 * (network players or AI) and informs specified {@link Game} class about it.
 * @author Petr Wudi
 */
public class NetworkClient implements Runnable {

    private Game game;
    private final String hostname;
    private final int port;
    private DataOutputStream output;
    private DataInputStream input;
    
    /**
     * true if the client is listening
     */
    private boolean isAlive;
    
    /**
     * @param hostname address of the another player
     * @param port port
     */
    public NetworkClient(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
        output = null;
        input = null;
        isAlive = true;
    }
  
    @Override
    public void run() {
        Socket socket = null;
        try {
            socket = new Socket(hostname, port);
            output = new DataOutputStream(socket.getOutputStream());
            input = new DataInputStream(socket.getInputStream());
            receiveMessages(input, output);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (output != null) {
                try { output.close(); } catch (IOException e) { /* NOP */ }
            }
            if (input != null) {
                try { input.close(); } catch (IOException e) { /* NOP */ }
            }
            if (socket != null) {
                try { socket.close(); } catch (IOException e) { /* NOP */ }
            }
        }
    }
    
    /**
     * Receives messages and reacts to them accordingly
     * @param input input stream
     * @param output output stream
     * @throws IOException 
     */
    private void receiveMessages(DataInputStream input, DataOutputStream output) throws IOException{
        while(isAlive){
            String answer = parseMessage(input.readUTF());
            if(answer.length() != 0){
                if(answer.charAt(0) == Messages.GAME_TERMINATED){
                    isAlive = false;
                    break;
                }
                System.out.println("Answer " + answer);
                output.writeUTF(answer);
            }
        }
    }
    
    /**
     * Reads message and makes response according to it
     * @param toParse received message
     * @return appropriate response to the received message
     */
    private String parseMessage(String toParse){
        System.out.println("To parse " + toParse);
        switch(toParse.charAt(0)){
            case Messages.GAME_START :
                if(game != null){
                    int[] size = parseIntPair(toParse);
                    game.generateGrid(size[0], size[1]);
                    return String.valueOf(Messages.ACCEPT);
                }
                break;
            case Messages.TILE_DISCOVERED :
                if(game != null){
                    int[] coords = parseIntPair(toParse);
                    game.discover(coords[0], coords[1]);
                }
                break;
            case Messages.BOMB_MARK :
                if(game != null){
                    int[] coords = parseIntPair(toParse);
                    game.markBomb(coords[0], coords[1]);
                }
                break;
            case Messages.PLAYER_PLACEMENT:
                if(game != null){
                    int[] coords = parseIntPair(toParse);
                    game.placePlayer(coords[0], coords[1]);
                    return String.valueOf(Messages.ACCEPT);
                }
                break;
            case Messages.GAME_START_TIME :
                game.start();
                break;
            case Messages.ADD_BOMB :
                if(game != null){
                    int[] coords = parseIntPair(toParse);
                    game.addBomb(coords[0], coords[1]);
                    return String.valueOf(Messages.ACCEPT);
                }
                break;
            case Messages.GAME_TERMINATED :
                return "" + Messages.SCORE + Messages.DELIMITER + Integer.toString(game.getScore());
            case Messages.GAME_RESULTS :
                toParse = toParse.substring(2);
                int orderStringEnd = toParse.indexOf(' '),
                    order = Integer.parseInt(toParse.substring(0, orderStringEnd));
                toParse = toParse.substring(orderStringEnd);
                game.end(order, toParse);
                return String.valueOf(Messages.GAME_TERMINATED);
        }
        return "";
    }
    
    /**
     * Parses integer pair (e.g. point coords) from the message
     * @param message message containing type of message (will be ignored), then
     * delimiter and the two integers delimited by {@link Messages#DELIMITER}.
     * Rest of the string will be ignored.
     * @return two-item array containing the parsed ints
     */
    protected int[] parseIntPair(String message){
        String[] strCoords = message.split(String.valueOf(Messages.DELIMITER));
        int[] result = new int[2];
        result[0] = Integer.parseInt(strCoords[1]); // strCoords[0] signalizes type of message
        result[1] = Integer.parseInt(strCoords[2]);
        return result;
    }
    
    /**
     * Sets game with which this class communicates
     * @param game the game
     */
    public void setGame(Game game){
        this.game = game;
    }
    
    /**
     * Marks specified tile as discovered
     * @param x horizontal position of the tile
     * @param y vertical position of the tile
     */
    public void markDiscovered(int x, int y) {
        String message = "" + Messages.TILE_DISCOVERED + Messages.DELIMITER +
                x + Messages.DELIMITER + y;
        sendMessage(message);
    }

    /**
     * Marks specified place as containing bomb
     * @param x horizontal position of the bomb
     * @param y vertical position of the bomb
     */
    public void markBomb(int x, int y) {
        String message = "" + Messages.BOMB_MARK + Messages.DELIMITER + x +
                Messages.DELIMITER + y;
        sendMessage(message);
    }

    /**
     * Quits the game
     */
    public void quitGame() {
        sendMessage(Messages.GAME_QUIT_OFFER);
    }
    
    /**
     * Sends message about player's death.
     */
    public void sendCapitulation() {
        sendMessage(Messages.CAPITULATION);
    }
    
    /**
     * Sends message to the server
     * @param message message to send
     */
    public void sendMessage(char message){
        sendMessage(String.valueOf(message));
    }
    
    /**
     * Sends message to the server
     * @param message message to send
     */
    public void sendMessage(String message){
       if(isAlive){
            try {
                output.writeUTF(message);
            } catch (IOException ex) {
                Logger.getLogger(NetworkClient.class.getName()).log(Level.SEVERE, null, ex);
            }
       }
    }
    
}
