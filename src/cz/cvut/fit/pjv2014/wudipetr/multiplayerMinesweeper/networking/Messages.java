package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.networking;

/**
 * Defines messaging protocol for network-related classes
 * @author Petr Wudi
 */
public class Messages {
    static final char GAME_START = 'g';
    static final char GAME_START_TIME = 's';
    static final char GAME_QUIT_OFFER = 'q';
    static final char GAME_TERMINATED = 'e';
    static final char GAME_RESULTS = 'x';
    static final char CAPITULATION = 'd';
    static final char BOMB_MARK = 'm';
    static final char ADD_BOMB = 'b';
    static final char PLAYER_PLACEMENT = 'p';
    static final char TILE_DISCOVERED = 't';
    static final char ACCEPT = 'a';
    static final char SCORE = 'r';
    static final char DELIMITER = ' ';
}
