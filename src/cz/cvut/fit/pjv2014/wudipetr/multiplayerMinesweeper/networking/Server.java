package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.networking;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.UiConstants;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Controls communication between players
 * @author Petr Wudi
 */
public class Server implements Runnable {
    
    private static final String SOCKET_CLOSE_ERROR = "Can't close server.";
    
    private final int port;
    private final int playerCount;
    private final int gameWidth;
    private final int gameHeight;
    private final int mineCount;
    private HashSet<SolitaireServer> childServers;
    private HashMap<SolitaireServer, Integer> results = null;
    private int notInitialized;
    private boolean gameFinished = false;
    
    /**
     * Creates new server
     * @param port port on which client and server communicates
     * @param playerCount number of players in game
     * @param gameWidth horizontal number of tiles in the game grid
     * @param gameHeight certical number of tiles in the game grid
     * @param bombCount number of count that should be in the game
     */
    public Server(int port, int playerCount, int gameWidth, int gameHeight, int bombCount) {
        this.port = port;
        this.playerCount = playerCount;
        this.gameWidth = gameWidth;
        this.gameHeight = gameHeight;
        this.mineCount = bombCount;
        childServers = new HashSet<>();
        notInitialized = playerCount;
    }
    
    @Override
    public void run() {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            Set bombs = generateBombs();
            int player = generatePlayer(bombs);
            for(int cliNum = 0; cliNum < playerCount; cliNum++){
                Socket socket = serverSocket.accept();
                SolitaireServer child = new SolitaireServer(socket, this, gameWidth, gameHeight, bombs, player);
                childServers.add(child);
                Thread solitaireServer = new Thread(child);
                solitaireServer.start();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (serverSocket != null) {
                try {
                    serverSocket.close();
                }
                catch (IOException e) {
                    System.err.println(SOCKET_CLOSE_ERROR);
                }
            }
        }   
    }
    
    /**
     * Marks another server as ready to play the game
     */
    public void nextChildReady(){
        if(--notInitialized == 0){
            String time = getTime() + 200;
            for(SolitaireServer child : childServers){
                child.startGame(time);
            }
        }
        
    }
    
    /**
     * @return current time
     */
    private String getTime(){
    	Calendar calendar = Calendar.getInstance();
    	return Long.toString(calendar.getTimeInMillis());
    }
    
    /**
     * Randomly puts bombs on some places on the grid
     * @return set of bomb positions (x * width + y)
     */
    public Set<Integer> generateBombs(){
        Set<Integer> bombs = new HashSet();
        int tileSum = gameWidth * gameHeight,
            mine;
        for(int numOfMines = gameWidth * gameHeight * mineCount / 100; numOfMines > 0; numOfMines--){
            do{
                mine = (int)(Math.random() * tileSum);
            }
            while(bombs.contains(mine));
            bombs.add(mine);
        }
        return bombs;
    }
    
    /**
     * Puts player on random place
     * @param bombs places to avoid
     * @return player's position (x * width + y), x = x position, y = y position
     */
    public int generatePlayer(Set bombs){
        int tileCount = gameWidth * gameHeight,
            playerPosition;
            do{
                playerPosition = (int)(Math.random() * tileCount);
            }
            while(bombs.contains(playerPosition));
        return playerPosition;
    }
    
    /**
     * Receives messages about game and then sends them to all children except
     * for the one that received it
     * @param sender server communicating with client, which sent the message
     * @param message received message
     */
    public synchronized void receiveMessage(SolitaireServer sender, String message) {
        if(message.charAt(0) == Messages.GAME_QUIT_OFFER){
            gameFinished = true;
            askForResults();
            return;
        }
        if(gameFinished){
            parseResults(sender, message);
            return;
        }
        for(SolitaireServer child : childServers){
            if(child != sender){
                child.sendMessage(message);
            }
        }
    }
  
    /**
     * Sends message asking for player's score to all players
     */
    private void askForResults(){
        for(SolitaireServer child : childServers){
            child.sendMessage(String.valueOf(Messages.GAME_TERMINATED));
        }
    }
    
    /**
     * Reads message and reacts to it accordingly
     * @param sender server communicating with client, which sent the message
     * @param message received message
     */
    private synchronized void parseResults(SolitaireServer sender, String message){
        if(message.charAt(0) != Messages.SCORE){
            return;
        }
        String[] messageParts = message.split(String.valueOf(Messages.DELIMITER));
        if(results == null){
            results = new HashMap<>();
        }
        if(results.containsKey(sender)){
            return;
        }
        results.put(sender, Integer.parseInt(messageParts[1]));
        if(results.size() >= playerCount){
            List score = sortMapSet(results.entrySet());
            String resultsString = resultsString(score);
            int order = 0;
            int lastScore = Integer.MAX_VALUE;
            Iterator<Map.Entry<SolitaireServer, Integer>> entries = score.iterator();
            while(entries.hasNext()){
                Map.Entry<SolitaireServer, Integer> entry = entries.next();
                int playerScore = entry.getValue();
                if(lastScore != playerScore){
                    ++order;
                }
                lastScore = playerScore;
                entry.getKey().sendLastMessage("" + Messages.GAME_RESULTS +
                        Messages.DELIMITER + order + resultsString);
            }
        }
    }
    
    /**
     * Sorts set of map entries
     * @param mapSet set of entries from results map
     * @return sorted set
     */
    private List sortMapSet(Set<Map.Entry<SolitaireServer, Integer>> mapSet){
        List<Map.Entry<SolitaireServer, Integer>> score =  new LinkedList<>(mapSet);
        Collections.sort(score, new Comparator<Map.Entry<SolitaireServer, Integer>>(){
            @Override
            public int compare(Map.Entry<SolitaireServer, Integer> first, Map.Entry<SolitaireServer, Integer> second){
                return second.getValue().compareTo(first.getValue());
            }
        });
        return score;
    }

    /**
     * Generates game result message
     * @param score score of all players
     * @return string about all score of all players
     */
    private String resultsString(List score) {
        String resultsString = "";
        Iterator<Map.Entry<SolitaireServer, Integer>> entries = score.iterator();
            int order = 0,
                lastScore = Integer.MAX_VALUE;
            while(entries.hasNext()){
                Map.Entry<SolitaireServer, Integer> entry = entries.next();
                int playerScore = entry.getValue();
                boolean sameOrderAsPrevious = true;
                if(lastScore != playerScore){
                    sameOrderAsPrevious = false;
                    ++order;
                }
                resultsString += UiConstants.GAME_RESULTS_PLAYER_POSITION_BEGIN +
                        Integer.toString(order) +
                        UiConstants.GAME_RESULTS_PLAYER_POSITION_END;
                if(sameOrderAsPrevious){
                    resultsString += UiConstants.GAME_RESULTS_SAME_AS_PREVIOUS;
                }
                lastScore = playerScore;
                if(playerScore < 0){
                    playerScore = -playerScore;
                    resultsString += UiConstants.GAME_RESULTS_KILLED;
                }
                resultsString += UiConstants.GAME_RESULTS_PLAYER_DELIMITER;
                if(playerScore == 0){
                    resultsString += UiConstants.GAME_RESULTS_NO_TILE;
                }
                else if(playerScore == 1){
                    resultsString += UiConstants.GAME_RESULTS_ONE_TILE;
                }
                else if(playerScore < 5){
                    resultsString += playerScore + UiConstants.GAME_RESULTS_THREE_TO_FIVE_TILES;
                }
                else{
                    resultsString += playerScore + UiConstants.GAME_RESULTS_MORE_TILES;
                }
            }
            return resultsString;
    }
    
}
