package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.networking;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Server which communicates with one specified client
 * @author Petr Wudi
 */
public class SolitaireServer implements Runnable {
    
    private final Socket socket;
    private final Server masterServer;
    private final int gameWidth;
    private final int gameHeight;
    private final Set<Integer> bombs;
    private int player;
    private DataOutputStream output;
    private DataInputStream input;
    
    /**
     * true if all the meta-information about game has been sent
     */
    private boolean gameStarted = false;
    
    /**
     * true if an message could be received
     */
    private boolean isAlive = true;

    public SolitaireServer(Socket socket, Server masterServer,
            int gameWidth, int gameHeight, Set bombs, int player) {
        this.socket = socket;
        this.masterServer = masterServer;
        this.gameWidth = gameWidth;
        this.gameHeight = gameHeight;
        this.bombs = bombs;
        this.player = player;
    }

    @Override
    public void run() {
        try {
            output = new DataOutputStream(socket.getOutputStream());
            input = new DataInputStream(socket.getInputStream());
            output.writeUTF("" + Messages.GAME_START + Messages.DELIMITER +
                    gameWidth + Messages.DELIMITER + gameHeight);
            waitForAcceptance();
            sendBombs();
            sendPlayerPosition();
            waitForAcceptance();
            masterServer.nextChildReady();
            receiveMessages();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (input != null){
                try {
                    input.close();
                } catch (IOException e) {
                    Logger.getLogger(SolitaireServer.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
    /**
     * Receive client's messages until game stopped
     * @throws IOException 
     */
    private void receiveMessages() throws IOException{
        String message;
        while(isAlive){
            try{
                message = input.readUTF();
            }
            catch(EOFException e){
                if(isAlive){
                    Logger.getLogger(SolitaireServer.class.getName()).log(Level.SEVERE, null, e);
                }
                return;
            }
            if(gameStarted){
                masterServer.receiveMessage(this, message);
            }
        }
    }
    
    /**
     * Waits until client sends acceptance
     * @throws IOException input error
     */
    private void waitForAcceptance() throws IOException{
        String acceptance = input.readUTF();
    }
    
    /**
     * Sends info about game start
     * @param time time game starts
     */
    public void startGame(String time){
        if(output != null){
            sendMessage("" + Messages.GAME_START_TIME + Messages.DELIMITER + time);
            gameStarted = true;
        }
    }
    
    /**
     * Sends info about all bombs to the another player
     * @throws IOException message can't be sent
     */
    private void sendBombs() throws IOException {
        for (Integer bomb : bombs) {
            String message = "" + Messages.ADD_BOMB + Messages.DELIMITER +
                    Integer.toString(bomb % gameHeight) + Messages.DELIMITER +
                    Integer.toString(bomb / gameHeight);
            output.writeUTF(message);
            waitForAcceptance();
        }
    }

    /**
     * Sends message to client
     * @param message message to send
     */
    public synchronized void sendMessage(String message) {
        try {
            output.writeUTF(message);
        } catch (IOException ex) {
            Logger.getLogger(SolitaireServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Sends message and quits connection
     * @param message message to send
     */
    protected synchronized void sendLastMessage(String message) {
        try {
            output.writeUTF(message);
        } catch (IOException ex) {
            Logger.getLogger(SolitaireServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            isAlive = false;
        }
    }
    
    /**
     * Stops receiving messages
     */
    protected void stopReceiving(){
        isAlive = false;
        input = null;
        output = null;
    }

    /**
     * Sends where player lies on the beginning of the game
     */
    private void sendPlayerPosition() {
        sendMessage("" + Messages.PLAYER_PLACEMENT + Messages.DELIMITER +
                    Integer.toString(player % gameHeight) + Messages.DELIMITER +
                    Integer.toString(player / gameHeight));
    }
}
