package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.fullscreen;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.JFrMainWindow;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Mouse listener used for message displayed on full screen
 * @author Petr Wudi
 */
public class FullscreenMouseListener implements MouseListener {

    JFrMainWindow window;
    
    /**
     * Creates mouse listener
     * @param window window which mouse listener affects
     */
    public FullscreenMouseListener(JFrMainWindow window) {
        this.window = window;
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        window.getContentPane().removeAll();
        window.displayMainMenu();
        window.removeMouseListener(this);
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
    
}
