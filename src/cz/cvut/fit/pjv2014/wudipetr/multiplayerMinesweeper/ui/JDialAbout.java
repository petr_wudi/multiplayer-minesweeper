package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * Window describing the game (about window)
 * @author Petr Wudi
 */
public class JDialAbout extends JDialog implements ActionListener {

    /**
     * Creates and displays about window
     * @param owner parent window
     */
    public JDialAbout(Frame owner) {
        super(owner, UiConstants.ABOUT_WINDOW_TITLE);
        JLabel icon = new JLabel("");
        icon.setIcon(new ImageIcon("images/logo.png"));
        JLabel name = new JLabel(UiConstants.ABOUT_NAME);
        name.setFont(name.getFont().deriveFont(18.0f));
        JLabel info = new JLabel(UiConstants.ABOUT_INFO);
        JLabel purpose = new JLabel(UiConstants.ABOUT_PURPOSE);
        JLabel author = new JLabel(UiConstants.ABOUT_AUTHOR);
        JButton jButClose = new JButton(UiConstants.ABOUT_CLOSE);
        jButClose.setActionCommand("");
        jButClose.addActionListener(this);
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(icon, BorderLayout.LINE_START);
        JPanel panel = new VerticalJPanel();
        name.setBorder(new EmptyBorder(0, 0, 5, 0));
        icon.setBorder(new EmptyBorder(0, 20, 0, 0));
        panel.setBorder(new EmptyBorder(20, 20, 10, 20));
        panel.add(name);
        panel.add(info);
        panel.add(purpose);
        panel.add(author);
        panel.add(jButClose);
        getContentPane().add(panel, BorderLayout.LINE_END);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        dispose();
    }
    
}
