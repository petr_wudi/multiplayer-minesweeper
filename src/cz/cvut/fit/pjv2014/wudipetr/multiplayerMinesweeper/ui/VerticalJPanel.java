package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui;

import java.awt.Component;
import java.awt.GridBagConstraints;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * JPanel that stacks all components vertically
 * @author Petr Wudi
 */
public class VerticalJPanel extends JPanel {

    private int y = 0;
    private static final GridBagConstraints gridConstraints = new GridBagConstraints();
    private final BoxLayout layout;
    
    static {
        gridConstraints.anchor = GridBagConstraints.WEST;
        gridConstraints.gridx = 0;
    }

    /**
     * Creates vertical JPanel
     */
    public VerticalJPanel() {
        super();
        this.layout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(layout);
    }
    
    @Override
    public Component add(Component component){
        gridConstraints.gridy = y++;
        if(component instanceof JComponent){
            ((JComponent) component).setAlignmentX(LEFT_ALIGNMENT);
        }
        add(component, gridConstraints);
        return component;
    }
    
}
