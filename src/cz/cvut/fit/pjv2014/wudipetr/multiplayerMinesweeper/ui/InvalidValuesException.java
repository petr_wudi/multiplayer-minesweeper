package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui;

/**
 * Exception that is used when there is an invalid value in game settings
 * @author Petr Wudi
 */
public class InvalidValuesException extends Exception {

    public InvalidValuesException(){
        super();
    }
    
    public InvalidValuesException(String alert) {
        super(alert);
    }
    
}
