package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game.Tile;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.fullscreen.FullscreenMessage;
import java.awt.Color;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * Loading window screen
 * @author Petr Wudi
 */
public class JLabLoading extends FullscreenMessage implements Runnable {

    private static final String ADDR_ANIMATION_BEGIN = "/cz/cvut/fit/pjv2014/wudipetr/multiplayerMinesweeper/images/loading";
    private static final String ADDR_ANIMATION_END = ".png";
    
    /**
     * Number of current image in animation
     */
    int animImageNum = 0;
    
    /**
     * Should animation run
     */
    boolean animShouldRun = false;
    
    /**
     * Animation frames
     */
    Icon[] images;
    
    /**
     * Number of frames in animation
     */
    private static final int NUMBER_OF_ICONS = 5;
    
    /**
     * Initializes the loading screen
     * @param singleGame is there an player to wait for. Texts on the screen
     * depend on this
     */
    public JLabLoading(boolean singleGame) {
        super(singleGame ? UiConstants.WAITING_AI : UiConstants.WAITING_NET,
                Color.white, new Color(0, 42, 128));
        images = loadImages();
        setOpaque(true);
        setIcon(images[0]);
        setHorizontalTextPosition(JLabel.CENTER);
        setVerticalTextPosition(JLabel.BOTTOM);
        setHorizontalAlignment(JLabel.CENTER);
    }
    
    /**
     * Loads animation images
     * @return array containing all the images. There is {@link #NUMBER_OF_ICONS}
     * images in the array.
     */
    private Icon[] loadImages(){
        Icon[] result = new Icon[NUMBER_OF_ICONS];
        for(int i = 0; i < NUMBER_OF_ICONS; ++i){
            String imgAddr = ADDR_ANIMATION_BEGIN + i + ADDR_ANIMATION_END;
            URL resource = Tile.class.getResource(imgAddr);
            result[i] = new ImageIcon(resource);
        }
        return result;
    }

    @Override
    public synchronized void run() {
        animShouldRun = true;
        while(animShouldRun){
            ++animImageNum;
            animImageNum %= NUMBER_OF_ICONS;
            setIcon(images[animImageNum]);
            repaint();
            try {
                wait(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(JLabLoading.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * Stops the animation on screen
     */
    public void stop(){
        animShouldRun = false;
    }

}
