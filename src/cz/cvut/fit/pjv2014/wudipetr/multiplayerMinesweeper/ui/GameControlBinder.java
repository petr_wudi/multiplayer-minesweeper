package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game.MoveDirection;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.players.Player;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

/**
 * Performs key or mouse binding on specified objects
 * @author Petr Wudi
 */
public class GameControlBinder {   
    
    /**
     * Codes for action identification
     */
    private static final String ACTION_LEFT = "l";
    private static final String ACTION_RIGHT = "r";
    private static final String ACTION_UP = "u";
    private static final String ACTION_DOWN = "d";
    private static final String ACTION_SHIFT_LEFT = "sl";
    private static final String ACTION_SHIFT_RIGHT = "sr";
    private static final String ACTION_SHIFT_UP = "su";
    private static final String ACTION_SHIFT_DOWN = "sd";
    
    /**
     * Adds key bindings to the component so it can control the player
     * @param component component to bind keys with
     * @param player controlled player
     */
    public static void bindKeys(JComponent component, Player player) {
        fillInputMap(component);
        fillActionMap(component, player);
    }

    /**
     * Puts key bindings to {@link InputMap} of specified component
     * @param component component containing the desired input map
     */
    private static void fillInputMap(JComponent component) {
        InputMap inputMap = component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        int noShiftCode = 0;
        int shiftCode = KeyEvent.SHIFT_DOWN_MASK;
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT,  noShiftCode), ACTION_LEFT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, noShiftCode), ACTION_RIGHT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP,    noShiftCode), ACTION_UP);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN,  noShiftCode), ACTION_DOWN);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT,  shiftCode), ACTION_SHIFT_LEFT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, shiftCode), ACTION_SHIFT_RIGHT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP,    shiftCode), ACTION_SHIFT_UP);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN,  shiftCode), ACTION_SHIFT_DOWN);
    }

    /**
     * Puts actions to {@link ActionMap} of specified component
     * @param component component containing the desired action map
     * @param player added actions are related to this player
     */
    private static void fillActionMap(JComponent component, Player player) {
        ActionMap actionMap = component.getActionMap();
        addGoToTileAction(actionMap, player, ACTION_SHIFT_LEFT,  MoveDirection.left);
        addGoToTileAction(actionMap, player, ACTION_SHIFT_RIGHT, MoveDirection.right);
        addGoToTileAction(actionMap, player, ACTION_SHIFT_UP,    MoveDirection.up);
        addGoToTileAction(actionMap, player, ACTION_SHIFT_DOWN,  MoveDirection.down);
        addMarkBombAction(actionMap, player, ACTION_SHIFT_LEFT,  MoveDirection.left);
        addMarkBombAction(actionMap, player, ACTION_SHIFT_RIGHT, MoveDirection.right);
        addMarkBombAction(actionMap, player, ACTION_SHIFT_UP,    MoveDirection.up);
        addMarkBombAction(actionMap, player, ACTION_SHIFT_DOWN,  MoveDirection.down);
    }


    /**
     * Adds action of going to another tile to the action map
     * @param actionMap action will be added to this map
     * @param player player who moves
     * @param action action code
     * @param direction direction to which player should go
     */
    private static void addGoToTileAction(ActionMap actionMap, Player player, String action, MoveDirection direction) {
        actionMap.put(action, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                 player.move(direction);
            }
        });
    }

    /**
     * Adds action of marking bomb to the action map
     * @param actionMap action will be added to this map
     * @param player player who marks the bomb
     * @param action action code
     * @param direction direction of the bomb relatively to current position
     */
    private static void addMarkBombAction(ActionMap actionMap, Player player, String action, MoveDirection direction) {
        actionMap.put(action, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                 player.markBomb(direction);
            }
        });
    }
    
}
