package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.mainmenu;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.UiConstants;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * Menu bar on top of window shown while main menu is displayed
 * @author Petr Wudi
 */
class MenuBar extends JMenuBar {

    /**
     * Generates menubar shown on upper part of window
     * @param al menu action listner
     */
    MenuBar(ActionListener al){
        JMenu menu = new JMenu(UiConstants.MENU_PROGRAM);
        menu.setMnemonic(KeyEvent.VK_P);
        add(menu);
        addItem(menu, UiConstants.MENU_SETTINGS, KeyEvent.VK_N, MainMenu.ACTION_MENU_SETTINGS, al);
        addItem(menu, UiConstants.MENU_ABOUT, KeyEvent.VK_P, MainMenu.ACTION_MENU_ABOUT, al);
        addItem(menu, UiConstants.MENU_QUIT, KeyEvent.VK_U, MainMenu.ACTION_MENU_QUIT, al);
    }
    
    /**
     * Adds an item to the menu bar
     * @param menu menu to add item to
     * @param name text shown on item button
     * @param mnemonic keyboard mnemonic for menu item
     * @param command command code to do
     */
    private void addItem(JMenu menu, String name, int mnemonic, String command, ActionListener al){
        JMenuItem menuItem = new JMenuItem(name, mnemonic);
        menuItem.addActionListener(al);
        menuItem.setActionCommand(command);
        menu.add(menuItem);
    }
    
}
