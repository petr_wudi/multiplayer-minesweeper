/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.fullscreen;

import java.awt.Color;
import javax.swing.JLabel;

/**
 * Message displayed on whole screen
 * @author Petr Wudi
 */
public class FullscreenMessage extends JLabel {

    /**
     * New full screen message
     * @param text text of message
     * @param fgColor text color
     * @param bgColor background color
     */
    public FullscreenMessage(String text, Color fgColor, Color bgColor) {
        super(text);
        setBackground(bgColor);
        setForeground(fgColor);
        setOpaque(true);
        setHorizontalTextPosition(JLabel.CENTER);
        setVerticalTextPosition(JLabel.BOTTOM);
        setHorizontalAlignment(JLabel.CENTER);
    }
}
