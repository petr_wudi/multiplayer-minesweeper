package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.mainmenu;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.VerticalJPanel;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.UiConstants;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

/**
 * Panel displaying main menu controls of computer enemy
 * @author Petr Wudi
 */
class AISettings extends VerticalJPanel {

    private int difficulty = 1;
    
    /**
     * Creates panel displaying AI controls
     * @param actionListener listener that is binded to this panel's components
     */
    AISettings(ActionListener actionListener){
        setBorder(BorderFactory.createTitledBorder(UiConstants.DIFFICULTY));
        JRadioButton button2 = new JRadioButton(UiConstants.DIFFICULTY_HARD);
        JRadioButton button1 = new JRadioButton(UiConstants.DIFFICULTY_MEDIUM);
        JRadioButton button0 = new JRadioButton(UiConstants.DIFFICULTY_EASY);
        button1.setSelected(true);
        ButtonGroup group = new ButtonGroup();
        button2.addActionListener(actionListener);
        button1.addActionListener(actionListener);
        button0.addActionListener(actionListener);
        button2.setActionCommand(MainMenu.ACTION_AI_DIFFICULTY_2);
        button1.setActionCommand(MainMenu.ACTION_AI_DIFFICULTY_1);
        button0.setActionCommand(MainMenu.ACTION_AI_DIFFICULTY_0);
        group.add(button2);
        group.add(button1);
        group.add(button0);
        add(button2);
        add(button1);
        add(button0);
    }

    /**
     * Sets how skilled the AI player will be
     * @param level difficulty of game
     */
    void setDifficulty(int level){
       difficulty = level; 
    }
    
    /**
     * @return level of AI player skills
     */
    int getDifficulty() {
        return difficulty;
    }
    
    
    
}
