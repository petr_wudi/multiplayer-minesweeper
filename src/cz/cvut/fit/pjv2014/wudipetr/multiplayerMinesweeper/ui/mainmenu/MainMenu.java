package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.mainmenu;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.MultiplayerMinesweeper;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.InvalidValuesException;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.JDialAbout;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.JFrMainWindow;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.JFrSettings;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.UiConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

/**
 * This class contains all parts of menu for game setting before running it.
 * @author Petr Wudi
 */
public class MainMenu implements ActionListener {
    
    static final String ACTION_MENU_ABOUT = "m_a";
    static final String ACTION_MENU_QUIT = "m_q";
    static final String ACTION_MENU_SETTINGS = "m_s";
    static final String ACTION_AI_DIFFICULTY_0 = "ai0";
    static final String ACTION_AI_DIFFICULTY_1 = "ai1";
    static final String ACTION_AI_DIFFICULTY_2 = "ai2";
    static final String ACTION_NET_ENEMY = "opponentNetEnemy";
    static final String ACTION_NET_ME = "opponentNetMe";
    static final String ACTION_OPPONENT_AI = "opponentAI";
    static final String ACTION_OPPONENT_NET = "opponentNet";
    static final String ACTION_RUN = "RUN";
    
    /**
     * panel with controls that configure AI player
     */
    AISettings aiSettings;
    
    /**
     * bar on the top of the window
     */
    JMenuBar menuBar;
    
    /**
     * panel with network setting controls
     */
    NetworkSettings networkSettings;
    
    /**
     * panel with controls that enables type of oponent decision (AI/net)
     */
    OpponentPanel opponentDecision;
    
    /**
     * panel with input fields that adjust game size
     */
    SizeSettings sizeSettings;
    
    /**
     * panel containing button that launches the game
     */
    JPanel submitButton;

    /**
     * window in which the menu should be displayed
     */
    JFrMainWindow window;
    
    private final MultiplayerMinesweeper controller;
    
    private int port = 1555;
    
    /**
     * number of bombs in the game
     */
    private int bombCount = 10;
    
    /**
     * Creates main menu with all it's components
     * @param controller class to delegate button press actions to
     */
    public MainMenu(MultiplayerMinesweeper controller) {
        JFrMainWindow.setMenuMode();
        this.controller = controller;
        menuBar = new MenuBar(this);
        opponentDecision = new OpponentPanel(this);
        sizeSettings = new SizeSettings();
        submitButton = generateSubmitButton();
        networkSettings = new NetworkSettings(this);
        networkSettings.setVisible(false);
        aiSettings = new AISettings(this);
    }
    
    /**
     * Displays the menu on specified window
     * @param window window to display menu on
     */
    public void display(JFrMainWindow window){
        this.window = window;
        window.setJMenuBar(menuBar);
        BoxLayout layout = new BoxLayout(window.getContentPane(), BoxLayout.PAGE_AXIS);
        window.getContentPane().setLayout(layout);
        window.addItem(sizeSettings);
        window.addItem(opponentDecision);
        window.addItem(networkSettings);
        window.addItem(aiSettings);
        window.addItem(submitButton);
    }

    /**
     * Creates submit button
     * @return panel with the button on it
     */
    private JPanel generateSubmitButton() {
        JPanel result = new JPanel();
        JButton jButSubmit = new JButton(UiConstants.PLAY);
        jButSubmit.setActionCommand(ACTION_RUN);
        jButSubmit.addActionListener(this);
        result.add(jButSubmit);
        return result;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case ACTION_RUN:
                controller.startGame(this, window);
                break;
            case ACTION_OPPONENT_AI:
                opponentDecision.setNetGame(false);
                aiSettings.setVisible(true);
                networkSettings.setVisible(false);
                window.pack();
                break;
            case ACTION_OPPONENT_NET:
                opponentDecision.setNetGame(true);
                aiSettings.setVisible(false);
                networkSettings.setVisible(true);
                window.pack();
                break;
            case ACTION_NET_ME:
                networkSettings.showMe();
                sizeSettings.setInputsEnabled(true);
                window.pack();
                break;
            case ACTION_NET_ENEMY:
                networkSettings.showEnemy();
                sizeSettings.setInputsEnabled(false);
                window.pack();
                break;
            case ACTION_AI_DIFFICULTY_0:
                aiSettings.setDifficulty(0);
                break;
            case ACTION_AI_DIFFICULTY_1:
                aiSettings.setDifficulty(1);
                break;
            case ACTION_AI_DIFFICULTY_2:
                aiSettings.setDifficulty(2);
                break;
            case ACTION_MENU_ABOUT:
                JDialAbout about = new JDialAbout(window);
                break;
            case ACTION_MENU_SETTINGS:
                JFrSettings settings = new JFrSettings(this);
                break;
            case ACTION_MENU_QUIT:
                window.close();
                break;
        }
    }
    
    /**
     * Sets number of bombs in game
     * @param newCount new number of bombs
     */
    public void setBombCount(int newCount) {
        bombCount = newCount;
    }

    /**
     * Sets port used for connection with another players
     * @param newPort new port
     */
    public void setPort(int newPort) {
        port = newPort;
    }
    
    /**
     * @return number of bombs in the game
     */
    public int getBombCount() {
        return bombCount;
    }

    /**
     * @return port used for connection with another players
     */
    public int getPort() {
        return port;
    }

    /**
     * @return horizontal number of tiles in the game
     * @throws cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.InvalidValuesException
     * width is lower than {@link MultiplayerMinesweeper#MIN_GAME_WIDTH} or
     * higher than {@link MultiplayerMinesweeper#MAX_GAME_WIDTH}
     */
    public int gameWidth() throws InvalidValuesException {
        return sizeSettings.gameWidth();
    }

    /**
     * @return vertical number of tiles in the game
     * @throws cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.InvalidValuesException
     * width is lower than {@link MultiplayerMinesweeper#MIN_GAME_HEIGHT} or
     * higher than {@link MultiplayerMinesweeper#MAX_GAME_HEIGHT}
     */
    public int gameHeight() throws InvalidValuesException {
        return sizeSettings.gameHeight();
    }

    /**
     * How many players will play the game
     * @return number of players
     * @throws cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.InvalidValuesException
     * number of players is negative or zero
     */
    public int playerCount() throws InvalidValuesException {
        return networkSettings.playerCount();
    }

    /**
     * @return true if this computer user sets the game, else if somebody else
     * does
     */
    public boolean ISetGame() {
        return networkSettings.ISetGame();
    }

    /**
     * @return true if game is played via network
     */
    public boolean netGameIsChosen() {
        return opponentDecision.netGameIsChosen();
    }

    /**
     * @return address of the enemy setting the game
     */
    public String getEnemyAddress() {
        return networkSettings.getEnemyAddress();
    }

    /**
     * @return level of AI player skills
     */
    public int getDifficulty() {
        return aiSettings.getDifficulty();
    }
    
}
