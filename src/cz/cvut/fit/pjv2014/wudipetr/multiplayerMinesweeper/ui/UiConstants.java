/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui;

/**
 * Constants used by UI
 * @author Petr Wudi
 */
public class UiConstants {
    public static final String GAME_OVER = "<html><b><center>"
                + "<font color=#0000ff bgcolor=#D3D3D3>&nbsp;&nbsp;Minesweeper&nbsp;&nbsp;</font></center>"
                + "Z důvodu výbuchu miny nemůžete pokračovat ve hře.<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "* &nbsp; Jestliže hrajete multiplayer, nezapomeňte pogratulovat protihráči.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "* &nbsp; Kliknutím na plochu zobrazíte dialog pro zahájení nové hry.<br><br>"
                + "Doufám, že vás hra i přes neúspěch bavila.</b></html>";
    public static final String GAME_WIN = "<html>Gratuluji k výhře<br>Výsledky:<br>";
    public static final String GAME_WIN_AFTER = "</html>";
    public static final String MAIN_MENU_WINDOW_TITLE = "Minesweeper";
    public static final String GAME_WINDOW_TITLE = "MultiplayerMinesweeper";
    public static final String ABOUT_WINDOW_TITLE = "MultiplayerMinesweeper";
    public static final String ABOUT_NAME = "Multiplayer Minesweeper";
    public static final String ABOUT_INFO = "Hra hledání min pro více hráčů";
    public static final String ABOUT_PURPOSE = "Semestrální práce pro předmět BI-PJV, zimní semestr 2014/2015";
    public static final String ABOUT_AUTHOR = "Autor: Petr Wudi";
    public static final String ABOUT_CLOSE = "Zavřít";
    public static final String GAME_END_BEGIN = "<html>Umístil(a) jste se na ";
    public static final String GAME_END_MIDDLE = ". místě<br>Výsledky:<br>";
    public static final String GAME_END_END = "</html>";
    public static final String GAME_RESULTS_SAME_AS_PREVIOUS = " (další)";
    public static final String GAME_RESULTS_KILLED = " (zabit)";
    public static final String GAME_RESULTS_PLAYER_POSITION_BEGIN = " - ";
    public static final String GAME_RESULTS_PLAYER_POSITION_END = ". hráč";
    public static final String GAME_RESULTS_PLAYER_DELIMITER = ": ";
    public static final String GAME_RESULTS_NO_TILE = "žádný objevený dílek";
    public static final String GAME_RESULTS_ONE_TILE = "1 objevený dílek<br>";
    public static final String GAME_RESULTS_THREE_TO_FIVE_TILES = " objevené dílky<br>";
    public static final String GAME_RESULTS_MORE_TILES = " objevených dílků<br>";
    public static final String SETTINGS_WINDOW_TITLE = "Nastavení - Minesweeper";
    public static final String SETTINGS_MINE_COUNT = "Hustota zaminování (kolik procent hrací plochy jsou miny)";
    public static final String SETTINGS_PORT = "Číslo portu (pro připojení přes síť, musí být stejné u Vás i všech protihráčů)";
    public static final String SETTINGS_MINE_COUNT_ERROR = "Hustota zaminování musí být větší než 0 % a menší než 95 %.";
    public static final String SETTINGS_PORT_ERROR = "Port může nabývat hodnot od 1024 do 65535.";
    public static final String SETTINGS_SUBMIT = "Potvrdit";
    public static final String SETTINGS_CLOSE = "Zrušit";
    public static final String WRONG_INPUT = "Neplatné údaje";
    public static final String WAITING_NET = "Čekám na spoluhráče...";
    public static final String WAITING_AI = "Načítám hru...";
    public static final String ERROR_JAVASTYLE = "Error loading java window style.";
    public static final String ERROR_NATIVESTYLE = "Error loading your OS-like window style. Default java style will be used instead.";
    public static final String ERROR_PLAYER_NUM = "Počet hráčů musí být kladný.";
    public static final String ERROR_SIZE = "Výška a šířka hrací plochy mohou nabývat hodnot od 5 do 100.";
    public static final String ERROR_ICONS = "Window icon image not found or not reachable. Details: ";
    public static final String DIFFICULTY = "Obtížnost hry";
    public static final String DIFFICULTY_HARD = "Těžká";
    public static final String DIFFICULTY_MEDIUM = "Střední";
    public static final String DIFFICULTY_EASY = "Na rozehřátí";
    public static final String MYIP = "Vaše lokální IP adresa:";
    public static final String MYIP_AFTER = "<html>Pokud jste s protihráčem"
                    + " oba připojeni ke<br>stejné síti, přepište tuto adresu do"
                    + " počítače<br>protihráče.</html>";
    public static final String ENEMY_IP = "Adresa protihráče";
    public static final String PLAYER_SUM = "Počet hráčů";
    public static final String SET = "Připojení se k protihráči";
    public static final String SET_ME = "Já vše nastavím, protihráč se připojí";
    public static final String SET_ENEMY = "Hru nastaví protihráč, já se jen připojím";
    public static final String PLAY = "Hrát";
    public static final String SIZE = "Velikost hry (počet políček)";
    public static final String WIDTH = "Šířka";
    public static final String HEIGHT = "Výška";
    public static final String GAMETYPE = "Typ hry";
    public static final String GAMETYPE_AI = "Proti počítači";
    public static final String GAMETYPE_NET = "Proti reálnému protihráči po síti";
    public static final String MENU_PROGRAM = "Program";
    public static final String MENU_SETTINGS = "Nastavení";
    public static final String MENU_ABOUT = "O programu";
    public static final String MENU_QUIT = "Ukončit program";
}
