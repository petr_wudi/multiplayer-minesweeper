package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.mainmenu;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.VerticalJPanel;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.InvalidValuesException;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.UiConstants;
import static java.awt.Component.RIGHT_ALIGNMENT;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * Panel displaying main menu controls to set networking
 * @author Petr Wudi
 */
class NetworkSettings extends VerticalJPanel {

    JRadioButton buttonSettingsMe;
    JRadioButton buttonSettingsEnemy;
    JTextField jTexEnemyAddress;
    JSpinner jSpPlayerCount;
    JPanel jPanNetworkSettingsMe;
    JPanel jPanNetworkSettingsEnemy;
    
    /**
     * Creates new network settings panel
     * @param actionListener action listener to bind button actions with
     */
    NetworkSettings(ActionListener actionListener){
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(UiConstants.SET));
        
        buttonSettingsMe = new JRadioButton(UiConstants.SET_ME);
        buttonSettingsEnemy = new JRadioButton(UiConstants.SET_ENEMY);
        buttonSettingsMe.setSelected(true);
        buttonSettingsMe.setActionCommand(MainMenu.ACTION_NET_ME);
        buttonSettingsEnemy.setActionCommand(MainMenu.ACTION_NET_ENEMY);
        buttonSettingsMe.addActionListener(actionListener);
        buttonSettingsEnemy.addActionListener(actionListener);
        
        jPanNetworkSettingsMe = new VerticalJPanel();
        
        JPanel jPanPlayerCount = new JPanel();
        JLabel jLabPlayerCount = new JLabel(UiConstants.PLAYER_SUM);
        jSpPlayerCount = new JSpinner();
        jSpPlayerCount.setValue(2);
        jLabPlayerCount.setLabelFor(jSpPlayerCount);
        jPanPlayerCount.setLayout(new GridLayout(1, 2));
        jPanPlayerCount.add(jLabPlayerCount);
        jPanPlayerCount.add(jSpPlayerCount);
        
        jPanNetworkSettingsEnemy = new VerticalJPanel();
        JLabel jLabEnemyAddress = new JLabel(UiConstants.ENEMY_IP);
        jTexEnemyAddress = new JTextField(10);
        jLabEnemyAddress.setLabelFor(jTexEnemyAddress);
        jPanNetworkSettingsEnemy.add(jLabEnemyAddress);
        jPanNetworkSettingsEnemy.add(jTexEnemyAddress);
        jPanNetworkSettingsEnemy.setVisible(false);
        
        JPanel jPanAddress = makeAddressNotification();
        jPanNetworkSettingsMe.add(jPanPlayerCount);
        jPanNetworkSettingsMe.add(jPanAddress);
        ButtonGroup group = new ButtonGroup();
        group.add(buttonSettingsMe);
        group.add(buttonSettingsEnemy);
        add(buttonSettingsMe);
        add(buttonSettingsEnemy);
        add(jPanNetworkSettingsMe);
        add(jPanNetworkSettingsEnemy);
    }
    
    /**
     * Makes panel informing user about his own address
     * @return panel with info about address on it
     */
    JPanel makeAddressNotification(){
        VerticalJPanel jPanAddress = new VerticalJPanel();
        JPanel jPanAddressCore = new JPanel(new GridLayout(1, 2));
        JLabel myAddressLabel = new JLabel(UiConstants.MYIP);
        JLabel myAddress = new JLabel(getLocalIP());
        jPanAddressCore.add(myAddressLabel);
        myAddressLabel.setAlignmentX(RIGHT_ALIGNMENT);
        myAddressLabel.setBorder(new EmptyBorder(5, 0, 5, 0));
        jPanAddressCore.add(myAddress);
        jPanAddress.add(jPanAddressCore);
        JLabel myAddressLabelAfter = new JLabel(UiConstants.MYIP_AFTER);
        jPanAddress.add(myAddressLabelAfter);
        return jPanAddress;
    }

    /**
     * @return local IP addresss
     */
    private String getLocalIP(){
        InetAddress ip;
        String result = "";
	try {
            ip = InetAddress.getLocalHost();
            result = ip.getHostAddress();
	} catch (UnknownHostException e) {
            e.printStackTrace();
	}
        return result;
    }

    /**
     * Shows all controls needed to set whole game and be the server.
     */
    void showMe() {
        jPanNetworkSettingsEnemy.setVisible(false);
        jPanNetworkSettingsMe.setVisible(true);
    }

    /**
     * Show all controls needed to connect to another computer.
     */
    void showEnemy() {
        jPanNetworkSettingsEnemy.setVisible(true);
        jPanNetworkSettingsMe.setVisible(false);
    }
    
    /**
     * How many players will play the game
     * @return number of players
     * @throws cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.InvalidValuesException
     * number of players is negative or zero
     */
    int playerCount() throws InvalidValuesException {
        int value = (Integer)jSpPlayerCount.getValue();
        if(value <= 0) {
            throw new InvalidValuesException(UiConstants.ERROR_PLAYER_NUM);
        }
        return value;
    }

    /**
     * @return true if this computer user sets the game, else if somebody else
     * does
     */
    boolean ISetGame() {
        return buttonSettingsMe.isSelected();
    }

    /**
     * @return address of the enemy setting the game
     */
    String getEnemyAddress() {
        return jTexEnemyAddress.getText();
    }
}
