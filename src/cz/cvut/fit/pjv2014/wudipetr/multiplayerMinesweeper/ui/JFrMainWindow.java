package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.mainmenu.MainMenu;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.MultiplayerMinesweeper;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Main window. The whole game, main menu and game results are displayed in this
 * window.
 * @author Petr Wudi
 */
public class JFrMainWindow extends JFrame {

    private static final String ADDRESS_ICON_16 = "/cz/cvut/fit/pjv2014/wudipetr/multiplayerMinesweeper/images/icon16.png";
    private static final String ADDRESS_ICON_32 = "/cz/cvut/fit/pjv2014/wudipetr/multiplayerMinesweeper/images/icon32.png";
    
    private final MultiplayerMinesweeper controller;
    MainMenu menu;
    private JLabLoading jLabLoading = null;
    private Thread jLabLoadingThread = null;
    
    /**
     * Creates main window, shows it and draws main menu there
     * @param controller game controller
     */
    public JFrMainWindow(MultiplayerMinesweeper controller) {
        super(UiConstants.MAIN_MENU_WINDOW_TITLE);
        menu = new MainMenu(controller);
        displayMainMenu();
        setLocationRelativeTo(null);
        setWindowIcons();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        this.controller = controller;
    }
    
    /**
     * Sets program icons
     */
    private void setWindowIcons(){
        ArrayList icons  = new ArrayList();
        try{
            icons.add(new ImageIcon(getClass().getResource(ADDRESS_ICON_16)).getImage());
            icons.add(new ImageIcon(getClass().getResource(ADDRESS_ICON_32)).getImage());
        }
        catch(java.lang.NullPointerException e){
            System.err.println(UiConstants.ERROR_ICONS + e.getMessage());
        }
        this.setIconImages(icons);
    }
    
    /**
     * Shows dialog informing user about wrong input
     * @param errorMsg info about the error
     */
    public void displayWrongInputError(String errorMsg){
        JOptionPane.showMessageDialog(
                this,
                errorMsg,
                UiConstants.WRONG_INPUT,
                JOptionPane.ERROR_MESSAGE
        );
    }
    
    /**
     * Makes window's look and feel to be able to show menu. It means it draws
     * all buttons and another components with OS design
     */
    public static void setMenuMode(){
        try {
           UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } 
        catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            System.err.println(UiConstants.ERROR_NATIVESTYLE);
        }
    }
    
    /**
     * Makes window's look and feel to be able to show game
     */
    public static void setGameMode(){
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } 
        catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            System.err.println(UiConstants.ERROR_JAVASTYLE);
        }
    }
    
    /**
     * Displays specified component on whole screen
     * @param onlyDisplaying component to display
     */
    public void displayOnly(JComponent onlyDisplaying){
        if(jLabLoading != null){
            jLabLoading.stop();
        }
        getContentPane().removeAll();
        setJMenuBar(null);
        setLayout(new GridLayout(1, 1));
        add(onlyDisplaying);
        revalidate();
    }

    /**
     * Displays window that informs player about game loading
     * @param singleGame is there another player to wait for
     */
    public void initalizeLoadingWindow(boolean singleGame) {
        jLabLoading = new JLabLoading(singleGame);
        displayOnly(jLabLoading);
        jLabLoadingThread = new Thread(jLabLoading);
        jLabLoadingThread.start();
    }
    
    /**
     * Adds an item to the window
     * @param component component to add
     */
    public void addItem(JComponent component){
        component.setAlignmentX(Component.CENTER_ALIGNMENT);
        component.setAlignmentY(Component.CENTER_ALIGNMENT);
        Dimension dim = component.getPreferredSize();
        dim.width = Integer.MAX_VALUE;
        component.setMaximumSize(dim);
        add(component);
    }

    /**
     * Closes the window and quits whole program
     */
    public void close() {
        dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    /**
     * Displays main menu
     */
    public void displayMainMenu() {
        setMenuMode();
        menu.display(this);
        pack();
    }
    
}
