package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.mainmenu;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.VerticalJPanel;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.UiConstants;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

/**
 * Part of main menu displaying controls to choose which game will player play
 * @author Petr Wudi
 */
class OpponentPanel extends VerticalJPanel {

    /**
     * True if game via internet is chosen, false if game against computer is
     * chosen
     */
    boolean netGameIsChosen = false;
    
    OpponentPanel(ActionListener actionListener){
        setBorder(BorderFactory.createTitledBorder(UiConstants.GAMETYPE));
        JRadioButton aiButton = new JRadioButton(UiConstants.GAMETYPE_AI);
        aiButton.setSelected(true);
        JRadioButton netButton = new JRadioButton(UiConstants.GAMETYPE_NET);
        ButtonGroup group = new ButtonGroup();
        group.add(aiButton);
        group.add(netButton);
        aiButton.setActionCommand(MainMenu.ACTION_OPPONENT_AI);
        netButton.setActionCommand(MainMenu.ACTION_OPPONENT_NET);
        aiButton.addActionListener(actionListener);
        netButton.addActionListener(actionListener);
        add(aiButton);
        add(netButton);
    }
    
    /**
     * Sets information about type of game
     * @param netGame true if net game is chosen, false if game against computer
     */
    void setNetGame(boolean netGame){
        netGameIsChosen = netGame;
    }

    /**
     * @return true if game is played via network
     */
    boolean netGameIsChosen() {
        return netGameIsChosen;
    }
    
}
