package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.mainmenu.MainMenu;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * Settings dialog
 * @author Petr Wudi
 */
public class JFrSettings extends JFrame implements ActionListener {

    private static String ACTION_SUBMIT = "s";
    
    private final MainMenu parent;
    private final JTextField jTFMineCount;
    private final JTextField jTFPort;

    /**
     * Shows settings dialog
     * @param parent all value changes will be communicated with the parent
     */
    public JFrSettings(MainMenu parent) {
        super(UiConstants.SETTINGS_WINDOW_TITLE);
        this.parent = parent;
        JLabel jLabMineCount = new JLabel(UiConstants.SETTINGS_MINE_COUNT);
        JLabel jLabPort = new JLabel(UiConstants.SETTINGS_PORT);
        jTFMineCount = new JTextField(Integer.toString(parent.getBombCount()));
        jTFPort = new JTextField(Integer.toString(parent.getPort()));
        JButton jButSubmit = new JButton(UiConstants.SETTINGS_SUBMIT);
        jButSubmit.setActionCommand(ACTION_SUBMIT);
        jButSubmit.addActionListener(this);
        JButton jButCancel = new JButton(UiConstants.SETTINGS_CLOSE);
        jButCancel.setActionCommand("");
        jButCancel.addActionListener(this);
        getContentPane().setLayout(new GridLayout(3, 2));
        getContentPane().add(jLabMineCount);
        getContentPane().add(jTFMineCount);
        getContentPane().add(jLabPort);
        getContentPane().add(jTFPort);
        getContentPane().add(jButSubmit);
        getContentPane().add(jButCancel);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String alert = "";
        if(ACTION_SUBMIT.equals(e.getActionCommand())){
            int port, mineCount;
            try{
                port = Integer.parseInt(jTFPort.getText());
            }
            catch(NumberFormatException ex){
                port = 0;
            }
            try{
                mineCount = Integer.parseInt(jTFMineCount.getText());
            }
            catch(NumberFormatException ex){
                mineCount = 0;
            }
            if(port >= 1024 && port <= 65535){
                parent.setPort(Integer.parseInt(jTFPort.getText()));
            }
            else{
                alert += UiConstants.SETTINGS_PORT_ERROR;
            }
            if(mineCount > 0 && mineCount < 95){
                parent.setBombCount(Integer.parseInt(jTFMineCount.getText()));
            }
            else{
                if(alert.length() > 0){
                    alert += "\n";
                }
                alert += UiConstants.SETTINGS_MINE_COUNT_ERROR;
            }
        }
        if(alert.length() == 0){
            dispose();
        }
        else{
            JOptionPane.showMessageDialog(this, alert, UiConstants.WRONG_INPUT, JOptionPane.ERROR_MESSAGE);
        }
    }
    
}
