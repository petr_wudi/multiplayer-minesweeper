package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.mainmenu;

import static cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.MultiplayerMinesweeper.MAX_GAME_HEIGHT;
import static cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.MultiplayerMinesweeper.MAX_GAME_WIDTH;
import static cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.MultiplayerMinesweeper.MIN_GAME_HEIGHT;
import static cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.MultiplayerMinesweeper.MIN_GAME_WIDTH;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.VerticalJPanel;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.InvalidValuesException;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.UiConstants;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;

/**
 *
 * @author Petr Wudi
 */
class SizeSettings extends VerticalJPanel {

    JSpinner widthInput;
    JSpinner heightInput;
    
    SizeSettings(){
        setBorder(BorderFactory.createTitledBorder(UiConstants.SIZE));
        widthInput = new JSpinner();
        widthInput.setValue(10);
        heightInput = new JSpinner();
        heightInput.setValue(10);
        JLabel widthLabel = new JLabel(UiConstants.WIDTH);
        JLabel heightLabel = new JLabel(UiConstants.HEIGHT);
        widthLabel.setLabelFor(widthInput);
        JPanel jPanWidth = new JPanel(new GridLayout(1, 2));
        jPanWidth.add(widthLabel);
        jPanWidth.add(widthInput);
        heightLabel.setLabelFor(heightInput);
        JPanel jPanHeight = new JPanel(new GridLayout(1, 2));
        jPanHeight.add(heightLabel);
        jPanHeight.add(heightInput);
        add(jPanWidth);
        add(jPanHeight);
    }

    /**
     * @return horizontal number of tiles in the game
     * @throws cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.InvalidValuesException
     * width is lower than {@link MultiplayerMinesweeper#MIN_GAME_WIDTH} or
     * higher than {@link MultiplayerMinesweeper#MAX_GAME_WIDTH}
     */
    int gameWidth() throws InvalidValuesException {
        int width = (Integer)widthInput.getValue();
        if(width < MIN_GAME_WIDTH || width > MAX_GAME_WIDTH){
            throw new InvalidValuesException(UiConstants.ERROR_SIZE);
        }
        return width;
    }

    /**
     * @return vertical number of tiles in the game
     * @throws cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.InvalidValuesException
     * width is lower than {@link MultiplayerMinesweeper#MIN_GAME_HEIGHT} or
     * higher than {@link MultiplayerMinesweeper#MAX_GAME_HEIGHT}
     */
    int gameHeight() throws InvalidValuesException {
        int height = (Integer)heightInput.getValue();
        if(height < MIN_GAME_HEIGHT || height > MAX_GAME_HEIGHT){
            throw new InvalidValuesException(UiConstants.ERROR_SIZE);
        }
        return height;
    }

    /**
     * Sets input fields enabled/disabled to write
     * @param areEnabled true if input fields should be enabled
     */
    void setInputsEnabled(boolean areEnabled) {
        widthInput.setEnabled(areEnabled);
        heightInput.setEnabled(areEnabled);
    }
}
