package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.players.Player;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game.Tile;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Listener used for clicking on tiles or for marking bombs
 * @author Petr Wudi
 */
public class ClickingListener implements MouseListener {

    Player player;
    
    /**
     * Creates new clicking listener
     * @param player player which discovers tiles and marks bombs
     */
    public ClickingListener(Player player) {
        this.player = player;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1){
            player.goTo((Tile)e.getComponent());
        }
        else if(e.getButton() == MouseEvent.BUTTON3){
            player.markMine((Tile)e.getComponent());
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
    
}
