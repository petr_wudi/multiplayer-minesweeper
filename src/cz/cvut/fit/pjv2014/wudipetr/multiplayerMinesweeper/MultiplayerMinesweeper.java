package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game.AIgame;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game.Game;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.networking.NetworkClient;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.networking.Server;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.InvalidValuesException;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.JFrMainWindow;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.mainmenu.MainMenu;

/**
 * Multiplayer minesweeper main class
 * @author Petr Wudi
 */
public class MultiplayerMinesweeper {

    public static String DEFAULT_OPPONENT_ADDRESS = "localhost";
    public static final int MIN_GAME_WIDTH = 5;
    public static final int MIN_GAME_HEIGHT = 5;
    public static final int MAX_GAME_WIDTH = 100;
    public static final int MAX_GAME_HEIGHT = 100;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MultiplayerMinesweeper mm = new MultiplayerMinesweeper();
        JFrMainWindow mainWindow = new JFrMainWindow(mm);
    }
    
    /**
     * Starts new game
     * @param menuControls menu with option buttons. Game options will be loaded
     * from the menu controls.
     * @param window window to display game in
     */
    public void startGame(MainMenu menuControls, JFrMainWindow window) {
        try {
            boolean isNetGame = menuControls.netGameIsChosen();
            int width = menuControls.gameWidth(),
                height = menuControls.gameHeight(),
                playerCount = (isNetGame ? menuControls.playerCount() : 2),
                port = menuControls.getPort(),
                bombCount = menuControls.getBombCount();
            boolean isSingleGame = !isNetGame || playerCount == 1;
            window.initalizeLoadingWindow(isSingleGame);
            String hostAddress = DEFAULT_OPPONENT_ADDRESS;
            if(menuControls.ISetGame()){
                Thread server = new Thread(new Server(port, playerCount, width, height, bombCount));
                server.start();
            }
            else if(hostAddress.trim().length() > 0){
                hostAddress = menuControls.getEnemyAddress();
            }   NetworkClient nc = new NetworkClient(hostAddress, port);
            Game game = new Game(nc, window);
            nc.setGame(game);
            Thread ncThread = new Thread(nc);
            ncThread.start();
            if(!isNetGame){
                int difficulty = menuControls.getDifficulty();
                NetworkClient ainc = new NetworkClient(hostAddress, port);
                Game aiGame = new AIgame(difficulty, width, height, ainc, window);
                ainc.setGame(aiGame);
                Thread aincThread = new Thread(ainc);
                aincThread.start();
            }
        } catch (InvalidValuesException ex) {
            window.displayWrongInputError(ex.getMessage());
        }
    }

}
