package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.players.AIPlayer;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.networking.NetworkClient;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.JFrMainWindow;

/**
 * Game against computer
 * @author Petr Wudi
 */
public class AIgame extends Game {

    /**
     * Thread manipulating with tiles (AI player)
     */
    Thread playerThread;
    
    /**
     * 
     * @param difficulty how difficult the game will be. Higher number means
     * more skilled AI player and therefore more difficult game. Allowed values
     * are between 0 and {@link AIPlayer#MAX_SKILL}.
     * @param width number of tiles in horizontal direction
     * @param height number of tiles in vertical direction
     * @param networkClient client to communicate results with
     * @param window window to display game in
     */
    public AIgame(int difficulty, int width, int height, NetworkClient networkClient, JFrMainWindow window) {
        super(networkClient, null);
        this.player = new AIPlayer(this, difficulty, width, height);
        playerThread = new Thread((Runnable) player);
    }
    
    @Override
    public void start(){
        playerThread.start();
    }
    
    @Override
    public void end(int order, String results){
        ((AIPlayer)player).stop();
    }
    
    @Override
    protected void checkEnd(){
    }
    
}
