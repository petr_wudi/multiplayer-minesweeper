package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.players.Player;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.networking.NetworkClient;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.fullscreen.FullscreenMessage;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.fullscreen.FullscreenMouseListener;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.JFrMainWindow;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.GameControlBinder;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.UiConstants;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Represents instance of multiplayer minesweeper game
 * @author Petr Wudi
 */
public class Game {
    
    /**
     * Grid containing all tiles
     */
    protected Grid grid;
    
    /**
     * This computer player
     */
    protected Player player;
    
    /**
     * Window in which game is displayed
     */
    protected JFrMainWindow window;
    
    /**
     * Drawing area in {@link JFrMainWindow} to show game in
     */
    protected JPanel drawingArea;
    
    /**
     * Client controlling all player instructions
     */
    protected NetworkClient networkClient;
    
    /**
     * Has the game been lost yet or not
     */
    protected boolean lost = false;
    
    /**
     * Creates new game (new instance of minesweeper)
     * @param networkClient client for communicating game results with
     * @param window window to diaply game in
     */
    public Game(NetworkClient networkClient, JFrMainWindow window) {
        this.networkClient = networkClient;
        this.window = window;
        if(window != null) window.setGameMode();
        drawingArea = new JPanel();
        player = new Player(this);
    }
    
    /**
     * Creates new grid with tiles
     * @param width number of tiles in horizontal direction
     * @param height number of tiles in vertical direction
     */
    public void generateGrid(int width, int height){
        drawingArea.setLayout(new GridLayout(height,width));
        grid = new Grid(width, height, drawingArea);
    }
    
    /**
     * @param x number of tile from left (including 0)
     * @param y number of tile from top (including 0)
     * @return tile at specified location
     */
    public Tile getTile(int x, int y){
        return grid.getTile(x, y);
    }
    
    /**
     * Moves player to specified direction
     * @param direction direction to move
     * @param player the player that is moving
     */
    public void movePlayer(MoveDirection direction, Player player){
        grid.movePlayer(direction, player);
    }
    
    /**
     * Mark tile next to the player as containing bomb
     * @param direction direction of neighbour containing bomb
     * @param player the player marking the bomb
     */
    public void markBomb(MoveDirection direction, Player player){
        grid.markBomb(direction, player);
    }
    
    /**
     * Adds bomb to tile with specified coords
     * @param x horizontal number of the tile
     * @param y vertical number of the tile
     */
    public void addBomb(int x, int y){
        grid.addBomb(x, y);
    }
    
    /**
     * Places player to tile with specified coords
     * @param x horizontal number of the tile
     * @param y vertical number of the tile
     */
    public void placePlayer(int x, int y){
        grid.placePlayer(player, x, y);
    }

    /**
     * Marks the tile as discovered and spreads the information to another
     * players
     * @param x horizontal position of the tile
     * @param y vertical position of the tile
     */
    public void announceDiscovery(int x, int y) {
        networkClient.markDiscovered(x, y);
        checkEnd();
    }

    /**
     * Marks specified tile as discovered
     * @param x horizontal position of the tile
     * @param y vertical position of the tile
     */
    public void discover(int x, int y) {
        grid.discover(x, y);
        checkEnd();
    }

    /**
     * Marks specified tile as containing bomb and spreads the information to
     * another players
     * @param x horizontal position of the bomb
     * @param y vertical position of the bomb
     */
    public void announceBomb(int x, int y){
        networkClient.markBomb(x, y);
    }
    
    /**
     * Marks specified tile as containing bomb
     * @param x horizontal position of the bomb
     * @param y vertical position of the bomb
     */
    public void markBomb(int x, int y){
        grid.markBomb(x, y);
    }
    
    /**
     * Checks whether all tiles has been discovered and quits game if so
     */
    protected void checkEnd(){
        if(grid.isGridDiscovered()){
            networkClient.quitGame();
        }
    }

    /**
     * @return score of the player
     */
    public int getScore() {
        return player.getScore();
    }

    /**
     * Sets game as lost.
     */
    public void setLost() {
        lost = true;
        networkClient.sendCapitulation();
    }

    /**
     * Quits the game and displays statistics
     * @param order how good the player was
     * @param results messsage to dispaly on screen
     */
    public void end(int order, String results) {
        JLabel jLabGameEnd;
        if(lost){
            jLabGameEnd = new FullscreenMessage(UiConstants.GAME_OVER, Color.lightGray, Color.blue);
        }
        else{
            if(order == 1){
                jLabGameEnd = new FullscreenMessage(UiConstants.GAME_WIN + results + UiConstants.GAME_WIN_AFTER, Color.white, new Color(73, 0, 182));
                jLabGameEnd.setIcon(new ImageIcon(getClass().getResource("/cz/cvut/fit/pjv2014/wudipetr/multiplayerMinesweeper/images/firstPlace.png")));
                
            }
            else{
                jLabGameEnd = new FullscreenMessage(UiConstants.GAME_END_BEGIN + order +
                            UiConstants.GAME_END_MIDDLE + results + UiConstants.GAME_END_END,
                            Color.white, Color.black);
            }
        }
        window.addMouseListener(new FullscreenMouseListener(window));
        window.displayOnly(jLabGameEnd);
    }

    /**
     * Starts the game
     */
    public void start() {
        GameControlBinder.bindKeys(drawingArea, player);
        window.displayOnly(drawingArea);
        drawingArea.revalidate();
        window.pack();
    }
}
