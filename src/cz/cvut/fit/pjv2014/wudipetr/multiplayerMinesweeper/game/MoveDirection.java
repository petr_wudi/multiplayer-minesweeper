/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game;

/**
 *
 * @author petr
 */
public enum MoveDirection {
    left, right, up, down;
}
