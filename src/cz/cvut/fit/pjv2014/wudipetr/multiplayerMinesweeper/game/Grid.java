package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game;

import static cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game.Tile.nullTile;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.players.Player;
import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.ui.ClickingListener;
import javax.swing.JPanel;

/**
 * Grid containing all tiles with bombs or numbers
 * @author Petr Wudi
 */
public class Grid {
    private int width, height;
    private Tile[][] tiles;
    private JPanel drawingArea;
    private Tile actualTile;
    private Tile lastNotDiscovered;
    
    /**
     * @param width number of tiles in horizontal direction
     * @param height number of tiles in vertical direction
     * @param drawingArea panel to draw tiles on
     */
    public Grid(int width, int height, JPanel drawingArea) {
        setAttributes(width, height, drawingArea);
        generateTiles();
    }
    
    /**
     * Sets the attributes
     * @param width number of tiles in horizontal direction
     * @param height number of tiles in vertical direction
     * @param drawingArea panel to draw tiles on
     */
    private void setAttributes(int width, int height, JPanel drawingArea) {
        this.width = width;
        this.height = height;
        this.drawingArea = drawingArea;
    }
    
    /**
     * Creates all the tiles
     */
    private void generateTiles(){
        tiles = new Tile[width][height];
        for(int wp = 0; wp < width; ++wp){
            for(int hp = 0; hp < height; ++hp){
                tiles[wp][hp] = new Tile(wp, hp);
                if(wp > 0){
                    tiles[wp][hp].setLeft(tiles[wp-1][hp]);
                    tiles[wp-1][hp].setRight(tiles[wp][hp]);
                }
                if(hp > 0){
                    tiles[wp][hp].setTop(tiles[wp][hp-1]);
                    tiles[wp][hp-1].setBottom(tiles[wp][hp]);
                }
            }
        }
        for(int hp = 0; hp < height; ++hp){
            for(int wp = 0; wp < width; ++wp){
                drawingArea.add(tiles[wp][hp]);
            }
        }
    }
    
    /**
     * Adds a bomb to specified location
     * @param x horizontal position of the bomb
     * @param y vertical position of the bomb
     */
    public void addBomb(int x, int y){
        tiles[x][y].addMine();
    }
    
    /**
     * @return true if there remains no tile undiscovered
     */
    public boolean isGridDiscovered(){
        while(lastNotDiscovered.isDiscovered() || lastNotDiscovered.containsBomb()){
            if(lastNotDiscovered.getXPosition() < width - 1){
                lastNotDiscovered = lastNotDiscovered.getRight();
            }
            else{
                int y = lastNotDiscovered.getYPosition();
                if(y <  height - 1){
                    lastNotDiscovered = tiles[0][++y];
                }
                else return true;
            }
        }
        return false;
    }
    
    /**
     * Places player on specified tile
     * @param player player to place in the grid
     * @param x horizontal position of the tile to place player on
     * @param y vertical position of the tile to place player on
     */
    public void placePlayer(Player player, int x, int y){
        lastNotDiscovered = tiles[0][0];
        actualTile = tiles[x][y];
        actualTile.goHere(player);
        ClickingListener clickingListener = new ClickingListener(player);
        for (Tile[] tileLine: tiles){           
            for (Tile tile: tileLine){
                tile.addMouseListener(clickingListener);
            }
        }
    }
    
    /**
     * Changes {@link #actualTile} to the current actual tile's neighbour in
     * specified direction. If there is no neighbour, current actual tille will
     * remain the same.
     * @param direction direction of neighbour of current actual tile
     */
    private void changeActualTile(MoveDirection direction){
        actualTile.leave();
        Tile newTile = actualTile.getNeighbour(direction);
        if(newTile != nullTile){
            actualTile = newTile;
        }
    }
    
    /**
     * Moves player from actual tile to tile next to it
     * @param direction direction of player's move
     * @param player the player to move
     */
    public void movePlayer(MoveDirection direction, Player player){
        changeActualTile(direction);
        actualTile.goHere(player);
    }
    
    /**
     * Marks tile next to actual one as containing bomb
     * @param direction tile marked as bomb is neighbour of player's current
     * position in this direction
     * @param player the player marking the bomb
     */
    public void markBomb(MoveDirection direction, Player player){
        changeActualTile(direction);
        actualTile.markBomb();
    }

    /**
     * Un-hides specified tile
     * @param x horizontal position of the tile
     * @param y vertical position of the tile
     */
    void discover(int x, int y) {
        tiles[x][y].discover();
    }

    /**
     * Marks tile as containing bomb
     * @param x horizontal position of the tile
     * @param y vertical position of the tile
     */
    void markBomb(int x, int y) {
        tiles[x][y].markBomb();
    }
    
    /**
     * @param x number of tile from left (including 0)
     * @param y number of tile from top (including 0)
     * @return tile at specified location
     */
    public Tile getTile(int x, int y){
        return tiles[x][y];
    }
}
