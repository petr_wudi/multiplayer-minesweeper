package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game;

/**
 * Tile that has no neighbours. Could not contain bomb.
 * @author Petr Wudi
 */
public class NullTile extends Tile {

    /**
     * Singleton - instance of NullTile
     */
    static NullTile instance = null;
    
    private NullTile() {
        super(-1, -1);
        isDiscovered = true;
    }
    
    /**
     * @return the instance of this class
     */
    public static NullTile getInstance(){
        if(instance == null){
            instance = new NullTile();
        }
        return instance;
    }
    
    
    
    @Override
    public int neighbourDiscoveredMineSum(){
        return 0;
    }
    
    @Override
    public int horizontalNeighboursDiscoveredMineSum(){
        return 0;
    }
    
    @Override
    public int horizontalNeighboursMineSum(){
        return 0;
    }
    
    @Override
    public void displayNumber(){
    }
    
    @Override
    protected void expandWhiteSpaceVertically(){
    }
    
    @Override
    protected void expandWhiteSpaceHorizontally(){
    }
    
    @Override
    protected void expandWhiteSpace(){
    }
    
    @Override
    protected int discoveredRanking(){
        return 1;
    }
    
    @Override
    protected int hExpandDiscoveredRanking(){
        return 4;
    }
}

