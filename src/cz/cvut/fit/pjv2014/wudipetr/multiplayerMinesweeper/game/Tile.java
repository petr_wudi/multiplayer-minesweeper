package cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.game;

import cz.cvut.fit.pjv2014.wudipetr.multiplayerMinesweeper.players.Player;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.Border;



/**
 * One part of game grid. Could contain bomb, could be discovered (and showing
 * number of bombs in neighbourhood) or marked as bomb.
 * @author Petr Wudi
 */
public class Tile extends JButton {
    
    /**
     * horizontal position of the tile
     */
    private final int x;
    
    /**
     * vertical position of the tile
     */
    private final int y;
    
    /**
     * width of the tile in pixels
     */
    public int width = 32;
    
    /**
     * height of the tile in pixels
     */
    public int height = 32;
    
    public static NullTile nullTile = NullTile.getInstance();
    
    /**
     * Left neighbour of this tile
     */
    protected Tile left = nullTile;
    
    /**
     * Right neighbour of this tile
     */
    protected Tile right = nullTile;
    
    /**
     * Upper neighbour of this tile
     */
    protected Tile top = nullTile;
    
    /**
     * Bottom neighbour of this tile
     */
    protected Tile bottom = nullTile;
    
    private boolean containsBomb;
    private int takenByPlayer;
    private boolean bombIsMarked = false;
    private final static Map<String, BufferedImage> images = new HashMap();
    private final static Map<String, ImageIcon> icons = new HashMap();
    private final static Border borderDesign = BorderFactory.createLineBorder(Color.LIGHT_GRAY);
    protected boolean isDiscovered = false;
    
    /**
     * Load number 0-8 images
     */
    static {
        for (int i = 0; i < 9; i++) {
            icons.put(Integer.toString(i),
                    new ImageIcon(Tile.class.getResource("/cz/cvut/fit/pjv2014/wudipetr/multiplayerMinesweeper/images/num" + i + ".png")));
        }
        icons.put("mark",
                new ImageIcon(Tile.class.getResource("/cz/cvut/fit/pjv2014/wudipetr/multiplayerMinesweeper/images/flag.png")));
        icons.put("mine",
                new ImageIcon(Tile.class.getResource("/cz/cvut/fit/pjv2014/wudipetr/multiplayerMinesweeper/images/mine.png")));
    }
    
    /***
     * @param x horizontal position of the tile
     * @param y vertical position of the tile
     */
    public Tile(int x, int y) {
        this.x = x;
        this.y = y;
        this.setBackground(Color.DARK_GRAY);
        this.setBorder(borderDesign);
        this.setFocusPainted(false);  
        this.setMinimumSize(new Dimension(32, 32));
    }
    
    /**
     * @return x position of the tile
     */
    public int getXPosition(){
        return x;
    }
    
    /**
     * @return y position of the tile
     */
    public int getYPosition(){
        return y;
    }
    
    /**
     * Sets left neighbour of this tile
     * @param left the neighbour
     */
    void setLeft(Tile left) {
        this.left = left;
    }
    
    /**
     * Sets right neighbour of this tile
     * @param right the neighbour
     */
    void setRight(Tile right) {
        this.right = right;
    }
    
    /**
     * Sets upper neighbour of this tile
     * @param top the neighbour
     */
    void setTop(Tile top) {
        this.top = top;
    }
    
    /**
     * Sets bottom neighbour of this tile
     * @param bottom the neighbour
     */
    void setBottom(Tile bottom) {
        this.bottom = bottom;
    }
    
    /**
     * @return number of bombs next to the tile
     */
    public int getNumber(){
        return left.bombCount() +
               right.bombCount() + 
               top.bombCount() +
               bottom.bombCount() + 
               top.horizontalNeighboursMineSum() + 
               bottom.horizontalNeighboursMineSum();
    }
    
    /**
     * Un-hides the tile
     */
    public void discover(){
        isDiscovered = true;
        if(bombIsMarked){
            displayBombMark();
            return;
        }
        if(containsBomb){
            displayBomb();
            return;
        }
        this.displayNumber();
        setBackground(Color.white);
    }
    
    /**
     * Moves player to the tile
     * @param player player to move here
     */
    public void goHere(Player player){
        discover();
        if(!bombIsMarked && containsBomb){
            player.die();
        }
    }
    
    /**
     * @return true if tile has been discovered by some player or contains bomb
     */
    public boolean tileTaken(){
        return containsBomb || takenByPlayer > 0;
    }
    
    /**
     * Displays number of the tile (number of neighbour bombs)s
     */
    public void displayNumber(){
        int number = getNumber();
        this.setIcon(icons.get(Integer.toString(number)));
        if(number == 0){
            expandWhiteSpaceVertically();
        }
    }
   
    /**
     * Displays bomb-mark on the tile
     */
    private void displayBombMark() {
        this.setIcon(icons.get("mark"));
        this.setBackground(Color.LIGHT_GRAY);
    }
    
    /**
     * Displays bomb on the tile
     */
    private void displayBomb(){
        this.setIcon(icons.get("mine"));
        this.setBackground(Color.PINK);
    }
    
    /**
     * Reveals all horizontal neighbours if this tile has no bomb neighbours.
     * Then it calls {@link #expandWhiteSpaceHorizontally() } on them
     */
    protected void expandWhiteSpaceVertically(){
        expandWhiteSpaceHorizontally();
        left.expandWhiteSpaceHorizontally();
        right.expandWhiteSpaceHorizontally();
        left.expandWhiteSpace();
        right.expandWhiteSpace();
    }
    
    /**
     * Reveals all horizontal neighbours if this tile has no bomb neighbours.
     */
    protected void expandWhiteSpaceHorizontally(){
        top.expandWhiteSpace();
        bottom.expandWhiteSpace();
    }
    
    /**
     * Reveals all neighbours if this tile has no bomb neighbours.
     */
    protected void expandWhiteSpace(){
        if(!isDiscovered){
            discover();
        }
    }
    
    public void addMine(){
        this.containsBomb = true;
    }
    
    public int neighbourDiscoveredMineSum(){
        return top.horizontalNeighboursDiscoveredMineSum()
            + bottom.horizontalNeighboursDiscoveredMineSum()
            + left.discoveredMineSum() + right.discoveredMineSum();
    }
    
    /*
    Returns how many mines are at left and right neighbours
    */
    public int horizontalNeighboursDiscoveredMineSum(){
        return left.discoveredMineSum() + right.discoveredMineSum();
    }
    
    public int discoveredMineSum(){
        return containsBomb && isDiscovered ? 1 : 0;
    }
    
    /*
    Returns how many mines are at left and right neighbours
    */
    public int horizontalNeighboursMineSum(){
        return left.bombCount() + right.bombCount();
    }
    
    /**
     * @return number of bombs on the tile
     */
    public int bombCount(){
        return containsBomb?1:0;
    }
    
    /**
     * @param direction direction of neighbour
     * @return neighbour of this tile in specified direction
     */
    public Tile getNeighbour(MoveDirection direction){
        switch(direction){
            case left:
                return this.left;
            case right:
                return this.right;
            case up:
                return this.top;
            case down:
                return this.bottom;
        }
        return nullTile;
    }
    
    /**
     * Marks current tile as mined
     */
    public void markBomb(){
        this.bombIsMarked = !this.bombIsMarked;
        isDiscovered = true;
        displayBombMark();
    }
        
    /**
     * Sets this tile as non-active
     */
    public void leave(){
        this.setBackground(Color.white);
    }
    
    /**
     * @return right neighbour of this tile
     */
    public Tile getRight(){
        return right;
    }

    /**
     * @return whether tile has been un-hidded yet
     */
    public boolean isDiscovered() {
        return isDiscovered;
    }

    /**
     * @return whether tile does contain bomb
     */
    public boolean containsBomb() {
        return containsBomb;
    }

    /**
     * @return how many tiles in 8x8 neighbourhood are discovered, 4x4 neighbous
     * are multiplied by 2
     */
    public int neighbourDiscoveredRanking() {
        int result = 0;
        result += left.discoveredRanking() * 2;
        result += right.discoveredRanking() * 2;
        
        
        result += bottom.hExpandDiscoveredRanking();
        result += top.hExpandDiscoveredRanking();
        return result;
    }
    
    /**
     * @return {@link discoveredRanking} of left and right neighbour plus 2 *
     * ranking of this tile
     */
    protected int hExpandDiscoveredRanking(){
        int result = 0;
        result += discoveredRanking() * 2;
        result += left.discoveredRanking();
        result += right.discoveredRanking();
        return result;
    }
    
    /**
     * @return 1 if discovered, 0 if not
     */
    protected int discoveredRanking(){
        return isDiscovered ? 1 : 0;
    }
    
    /**
     * Finds an tile next to this one, that does not containing bomb
     * @return the not-bombed tile
     */
    public Tile neighbourFreeNotBomb() {
        if(getNumber() < neighbourDiscoveredMineSum()){
            if(!left.isDiscovered()){
                return left;
            }
            if(!right.isDiscovered()){
                return right;
            }
            if(!bottom.isDiscovered()){
                return bottom;
            }
            if(!top.isDiscovered()){
                return top;
            }
        }
        return null;
    }
    
}
